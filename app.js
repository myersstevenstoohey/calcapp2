const express = require('express');
const router = express.Router();
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const sgMail = require('@sendgrid/mail');
var today = new Date();
var mos = ["Jan","Feb","Mar","Apr","May","Jun","Jul" ,"Aug","Sep", "Nov","Dec"];
var d = today.getDay();
var y = today.getUTCFullYear();
var toDate = mos[today.getMonth()] + " " + d + ", " + y;
sgMail.setApiKey("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")

//Function to Create Email
var createEmail = function(dataObj){
    var msg = {
          to: [
              {
                  "email":"writeseanhong@gmail.com",
                  "name" : "Sean Hong"
              },
              {
                "email":"writevictoria@gmail.com",
                "name" : "Victoria Hong"
              },
              {
                "email":"shong@myers-stevens.com",
                "name" : "shong"
              }
          ],
          from: 'CalcApp@gmail.com',
          subject: 'Calc App Report Log for ' + toDate + ' By ' + dataObj.fullName,
          html: logParser(dataObj)
        }
        return msg
}

// Function to parse the string into html table format
var logParser = function(dataObj){
    var log = dataObj.calcs.split(",")
    var tableStarter = "<table><tr><th>Date Received :</th><th>" + dataObj.recDate + "</th></tr>";
    var tableEnd = "<tr><td><b>Total: </b></td><td><b>" + dataObj.theTotal + "</b></td></tr>";
    //for loop to iterate through array
    for (var i = 0; i < log.length; i++){
        tableStarter += '<tr><td>' + log[i] + '</td></tr>'
    }
    //Need total
    //future version...
    //conditional to check if it exceeds certain number of td
    //if it does, create tr, flag variable to reset after certain amount of iteration
    return tableStarter + tableEnd;
}

// View Engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname,'views'));

//Static Folder set up
app.use(express.static(path.join(__dirname,'public')));

//Body Parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.get('/', function(req, res){
    console.log('Hello, Welcome Back')
})

app.post('/report', function(req,res){
    var report = {
        fullName: req.body.fullName,
        recDate: req.body.recDate,
        calcDate: toDate,
        calcs: req.body.calcs,
        theTotal: req.body.total
    }
    // console.log(createEmail(report)); // only for testing...
    sgMail.send(createEmail(report));
    res.redirect('/');
})

app.listen(3000, function(){
    console.log('server starting', toDate);
})

module.exports = router;
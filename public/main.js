var total = 0;
var lastNum = "";
var decimalFlag = false;
var minusFlag = false;

document.onkeyup = function(e) {
    var keyStorage = e.key;
    switch(keyStorage){
        case "0":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "1":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "2":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "3":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "4":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "5":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "6":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "7":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "8":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "9":
            //do something
            lastNum += e.key;
            showScreen(lastNum);
            break;
        case "Enter":
            if (lastNum && !isNaN(lastNum)) {
                lastNum = document.querySelector('#screen').innerHTML;
                total += parseFloat(lastNum);
                document.querySelector('#total').innerHTML = total.toLocaleString();
                if (!minusFlag) {
                    addLog(lastNum.toLocaleString() + " +");
                } else {
                    addLog(lastNum.toLocaleString());
                }
                showScreen("");
                lastNum = "";
                document.querySelector('#view_log_back').scrollTop = document.querySelector('#view_log_back').scrollHeight;
                decimalFlag = false;
                minusFlag = false;
            }
            break;
        case "-":
            //do minus
            if(!minusFlag){
                minusFlag = true;
                lastNum += e.key;
                showScreen(lastNum);
            }
            //possible function to fetch previous number
            break;
        case "Backspace":
            //do something
            lastNum = lastNum.slice(0,-1);
            showScreen(lastNum);
            break;
        case ".":
            //do something
            if (!decimalFlag){
                //if flag variable is false, add decimal
                decimalFlag = true;
                lastNum += e.key;
                showScreen(lastNum);
            }
            break;
        default:
            //do nothing
            break;

    }
};

//function to take the value of top div and display on key press function call
var showScreen = function(someValue){
    document.querySelector('#screen').innerHTML = someValue;
};

//Takes the input at top, creates an li and appends it to the html ul #log

var addLog = function(theKey){
    var log = document.createElement("li");
    var input = document.createTextNode(theKey);
    if(minusFlag){
        log.className = "red";
    }
    log.appendChild(input);
    document.querySelector('#log').appendChild(log);
};

////////////////////Send functionality
//grab variables function
var fetchName = function(){
  return document.querySelector('#name').innerHTML;
};
var fetchDate = function (){
    return document.querySelector('#theTime').innerHTML;
};

//create jspdf function
// var createPDF = function(){
//     var doc = new jsPDF();
//     doc.text('' + fetchName(), 100,100);
//     doc.text('' + fetchDate(), 200, 200);
//     doc.text('this is something really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really really long.',0,0);
//     doc.save('testing.pdf')
// };
//email function

var sendButton = function (){
    console.log('sending', fetchName());
    createPDF();

};